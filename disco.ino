#include <GyverButton.h>

#define BR_MIN  10
#define BR_MAX  200
#define SPEED 10

#define ANALOG_PIN0  A0
#define ANALOG_PIN1  A2
#define ANALOG_PIN2  A3
#define ANALOG_PIN3  A5
#define BTN_0      3
#define BTN_1      6

GButton btn0(BTN_0);
GButton btn1(BTN_1);

#define NUM_LEDS 48        // количество светодиодов (данная версия поддерживает до 410 штук)
#define CURRENT_LIMIT 3000  // лимит по току в МИЛЛИАМПЕРАХ, автоматически управляет яркостью (пожалей свой блок питания!) 0 - выключить лимит
byte BRIGHTNESS = 200;

#define LED_PIN 12

#define FASTLED_ALLOW_INTERRUPTS 1
#include "FastLED.h"
int analogData0 = 0;
int analogData1 = 0;
int analogData2 = 0;
int analogData3 = 0;
CRGB leds[NUM_LEDS];
float leds_brightness[NUM_LEDS];
byte leds_color[NUM_LEDS];
int leds_direction[NUM_LEDS];
int color_direction[NUM_LEDS];

void setup() {
  pinMode(ANALOG_PIN0, INPUT);
  pinMode(ANALOG_PIN1, INPUT);
  pinMode(ANALOG_PIN2, INPUT);
  pinMode(ANALOG_PIN3, INPUT);
  pinMode(BTN_0, INPUT_PULLUP);
  pinMode(BTN_1, INPUT_PULLUP);




  Serial.begin(9600);


  FastLED.addLeds<WS2811, LED_PIN, GRB>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  if (CURRENT_LIMIT > 0) FastLED.setMaxPowerInVoltsAndMilliamps(5, CURRENT_LIMIT);
  FastLED.setBrightness(BRIGHTNESS);

  for (int i = 0; i < NUM_LEDS; i++) {
    leds_brightness[i] = random(BR_MIN, BR_MAX);
    leds_color[i] = random(0, 255);
    leds_direction[i] = -1;
    color_direction[i] = -1;
    leds[i] = CHSV(leds_color[i], 200, leds_brightness[i]);
  }

}

void loop() {
  btn0.tick();
  btn1.tick();
  readValues();

  crazyDance();


}

void crazyDance() {
  byte color = map(analogData0, 0, 1024, 20, 234);
  byte range = map(analogData3, 0, 1024, 0, 10);
  byte colorMin = color - 20;
  byte colorMax = color + 20;

  for (int i = 0; i < NUM_LEDS; i++) {

    if (leds_brightness[i] >= BR_MAX || leds_brightness[i] <= BR_MIN) {
      leds_direction[i] = leds_direction[i] * -1;
    }

    leds_brightness[i] = leds_brightness[i] + leds_direction[i] * random(0, range);


    if (leds_color[i] <= colorMin && color_direction[i] < 0) {
      color_direction[i] = 1;
      leds_color[i] = colorMin;
    }

    if (leds_color[i] >= colorMax && color_direction[i] > 0) {
      color_direction[i] = -1;
      leds_color[i] = colorMax;
    }

    leds_color[i] = leds_color[i] + color_direction[i] * random(0, range);

    leds[i] = CHSV(leds_color[i], 200, leds_brightness[i]);
  }


  int brightness = map(analogData1, 0, 1024, 0, 255);
  int duration = map(analogData2, 0, 1024, 0, 100);
  Serial.println(color);
  FastLED.setBrightness(brightness);
  FastLED.show();
  delay(duration);
}


void readValues() {
  analogData0 = analogRead(ANALOG_PIN0);
  analogData1 = analogRead(ANALOG_PIN1);
  analogData2 = analogRead(ANALOG_PIN2);
  analogData3 = analogRead(ANALOG_PIN3);

  //  Serial.print(analogData0);
  //  Serial.print(" ");
  //  Serial.print(analogData1);
  //  Serial.print(" ");
  //  Serial.print(analogData2);
  //  Serial.print(" ");
  //  Serial.println(analogData3);
  if (btn0.isClick())
    Serial.println("Click 0");
  if (btn1.isClick())
    Serial.println("Click 1");
}
